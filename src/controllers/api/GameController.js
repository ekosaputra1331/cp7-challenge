const ValidationError = require("../../utils/ValidationError");
const { models, sequelize } = require("../../../models/index");
const response = require("../../utils/response");

/*
  @desc   getting all games
  @route  GET /api/v1/games
  @access public
*/
async function index(req, res) {
  try {
    const games = await models.Game.findAll({});
    return response.success(res, "fetching games", games);
  } catch (error) {
    return response.error(res, error);
  }
}

/*
  @desc   getting a game
  @route  GET /api/v1/games/:id
  @access public
*/
async function show(req, res) {
  if (!req.params.id) return response.error(res, "Game id not specified", 400);

  try {
    const game = await models.Game.findByPk(req.params.id);
    if (!game) throw new ValidationError("Game not found", 400);

    return response.success(res, "feching a game", game);
  } catch (error) {
    return response.error(res, error);
  }
}

/*
  @desc   create a game
  @route  GET /api/v1/games
  @access public
*/
async function create(req, res) {
  const { name, description } = req.body;

  if (!name || !description)
    return response.error(res, "Missing credentials", 400);

  try {
    const game = await models.Game.create(
      {
        name,
        description,
      },
      {
        returning: true,
      }
    );

    return response.success(res, "created game successfully", game, 201);
  } catch (error) {
    return response.error(res, error);
  }
}

/*
  @desc   update a game
  @route  PUT /api/v1/games/:id
  @access public
*/
async function update(req, res) {
  if (!req.params.id) return response.error(res, "Game id not specified");

  const { name, description } = req.body;

  try {
    const game = await models.Game.findByPk(req.params.id);
    if (!game) throw new ValidationError("Game not found", 400);

    const [_, [updatedGame]] = await models.Game.update(
      {
        name: name ?? game.name,
        description: description ?? game.description,
      },
      {
        where: {
          id: game.id,
        },
        returning: true,
      }
    );
    return response.success(res, "game updated successfully", updatedGame);
  } catch (error) {
    return response.error(res, error);
  }
}

/*
  @desc   delete a game
  @route  DELETE /api/v1/games/:id
  @access public
*/
async function destroy(req, res) {
  if (!req.params.id) return response.error(res, "Game id not specified");

  try {
    const game = await models.Game.findByPk(req.params.id);

    if (!game) throw new ValidationError("Game not found", 400);

    await models.Game.destroy({
      where: {
        id: req.params.id,
      },
    });

    return response.success(res, "game destroyed successfully", game);
  } catch (error) {
    return response.error(res, error);
  }
}

module.exports = {
  index,
  show,
  create,
  update,
  destroy,
};
