const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const util = require("util");

const response = require("../../utils/response");
const { models, sequelize } = require("../../../models/index");
const ValidationError = require("../../utils/ValidationError");
const jwtSignAsync = util.promisify(jwt.sign);

/*
  @desc   login user
  @route  GET /api/v1/users/login
  @access public
*/
async function login(req, res) {
  const { username, password } = req.body;

  if(!username || !password){
    return response.error(res, "Missing credentials")
  }

  try {
    const user = await models.User.findOne({
      where: {
        username,
      }
    });

    if (!user || !(await bcrypt.compare(password, user.password))) {
      throw new Error("Invalid credentials");
    }
    const token = await jwtSignAsync({
      sub: user.id,
      iss: 'chapter7',
      aud: 'resful'
    }, process.env.JWT_SECRET,{
      expiresIn: '1h'
    });

    return response.success(res, 'user logged in successfully', {
      id: user.id,
      username: user.username,
      token
    });
  } catch (error) {
    return response.error(res, error);
  }
}

/*
  @desc   index page
  @route  GET /api/v1/users
  @access public
*/
async function index(req, res) {
  try {
    const users = await models.User.findAll({
      include: {
        model: models.UserBio,
        as: "bio",
        attributes: {
          exclude: ["id", "user_id"],
        },
      },
      attributes: {
        exclude: ["password"],
      },
      order: [["id", "asc"]],
    });

    return response.success(res, "fetching users", users);
  } catch (error) {
    return response.error(res, error);
  }
}

/*
  @desc   show user
  @route  GET /api/v1/users/:id
  @access public
*/
async function show(req, res) {
  try {
    const user = await models.User.findByPk(req.params.id, {
      include: [
        {
          model: models.UserBio,
          as: "bio",
          attributes: {
            exclude: ["id", "user_id"],
          },
        },
        {
          model: models.UserGameHistory,
          as: "game_histories",
          attributes: {
            exclude: ["id", "game_id", "user_id"],
          },
          include: [
            {
              model: models.Game,
              as: "game",
            },
          ],
        },
      ],
      attributes: {
        exclude: ["password"],
      },
      order: [
        [
          { model: models.UserGameHistory, as: "game_histories" },
          "played_at",
          "DESC",
        ],
      ],
    });

    if (!user) {
      throw new ValidationError("User does not exist");
    }

    return response.success(res, "fetching a user", user);
  } catch (error) {
    return response.error(res, error);
  }
}

/*
  @desc   create user
  @route  POST /api/v1/users
  @access public
*/
async function create(req, res) {
  const { username, password, first_name, last_name, hobby, address } =
    req.body;

  if (!username || !password || !first_name || !last_name) {
    return response.error(res, "Missing credentials");
  }

  try {
    const user = await models.User.count({
      where: {
        username,
      },
    });

    if (user >= 1) {
      throw new Error("User already exists");
    }

    const salt = await bcrypt.genSalt(parseInt(process.env.SALT));
    const hashPassword = await bcrypt.hash(password, salt);

    const result = await sequelize.transaction(async (transaction) => {
      const newUser = await models.User.create(
        {
          username,
          password: hashPassword,
          role: "player",
        },
        {
          transaction,
          returning: true,
        }
      );

      await models.UserBio.create(
        {
          user_id: newUser.id,
          first_name,
          last_name,
          hobby,
          address,
        },
        {
          transaction,
        }
      );

      return newUser;
    });
    return response.success(res, "user created successfully", {
      id: result.id,
      username: result.username,
      role: result.role,
    });
  } catch (error) {
    return response.error(res, error);
  }
}

/*
  @desc   update user
  @route  PUT /api/v1/users/:id
  @access public
*/
async function update(req, res) {
  if (!req.params.id) {
    return response.error(res, "User id is required");
  }

  const { first_name, last_name, address, hobby } = req.body;

  try {
    const result = await sequelize.transaction(async (transaction) => {
      const userBio = await models.UserBio.findOne({
        where: {
          user_id: req.params.id,
        },
        transaction,
        lock: true,
      });

      if (!userBio) {
        throw new Error("UserBio not found");
      }

      const [_, [updatedUserBio]] = await models.UserBio.update(
        {
          first_name: first_name ?? userBio.first_name,
          last_name: last_name ?? userBio.last_name,
          address: address ?? userBio.address,
          hobby: hobby ?? userBio.hobby,
        },
        {
          where: {
            id: userBio.id,
          },
          transaction,
          returning: true,
        }
      );

      return updatedUserBio;
    });

    return response.success(res, "user has been updated successfully", result);
  } catch (error) {
    return response.error(res, error);
  }
}

/*
  @desc   delete user
  @route  DELETE /api/v1/users/:id
  @access public
*/
async function destroy(req, res) {
  if (!req.params.id) return response.error(res, "User id is required");

  try {
    const result = await sequelize.transaction(async (transaction) => {
      const userBio = await models.UserBio.destroy({
        where: {
          user_id: req.params.id,
        },
        transaction,
      });

      if (userBio === 0) throw new Error("User Bio not found");

      await models.UserGameHistory.destroy({
        where: {
          user_id: req.params.id,
        },
        transaction
      })

      const user = await models.User.findByPk(req.params.id, {
        transaction,
      });

      if (!user || user.role === "admin")
        throw new Error("User not found or cannot be deleted");

      await models.User.destroy({
        where: {
          id: req.params.id,
        },
        transaction,
      });

      return user;
    });

    return response.success(
      res,
      result.username + " has been deleted successfully",
      {
        username: result.username,
      }
    );
  } catch (error) {
    return response.error(res, error);
  }
}


module.exports = {
  index,
  show,
  create,
  update,
  destroy,
  login
};
