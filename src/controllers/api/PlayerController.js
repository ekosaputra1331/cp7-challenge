const { models, sequelize } = require("../../../models/index");
const response = require("../../utils/response");
const ValidationError = require("../../utils/ValidationError");

/*
  @desc   players fighting
  @route  POST /api/v1/players/fight/:roomId
  @access private
*/
async function fighting(req, res) {
  const roomId = req.params.roomId;
  const { data } = req.body;

  if (isNaN(parseInt(roomId))) {
    return response.error(res, "Invalid roomId");
  }

  if (!data || !isValidData(data)) {
    return response.error(res, "Invalid data");
  }

  try {
    const room = await models.Room.findByPk(roomId, {
      include: {
        model: models.Game,
        as: "game",
      },
    });

    if (!room) {
      throw new ValidationError("Room not found");
    }
    const max_player = await models.Player.findAll({
      where: {
        room_id: room.id,
      },
    });

    if (!room.isActive || max_player.length === room.game.max_player) {
      throw new ValidationError("Room reached maximum player or not active");
    }

    const [player, created] = await models.Player.findOrCreate({
      where: {
        room_id: roomId,
        player_id: req.user.id,
      },
      defaults: {
        room_id: roomId,
        player_id: req.user.id,
        data,
      },
    });

    if (!created) {
      throw new ValidationError("You already sent data to this room");
    }

    return response.success(res, "player created successfully", player, 201);
  } catch (error) {
    return response.error(res, error);
  }
}

function isValidData(data) {
  const validData = ["R", "P", "S", "r", "p", "s"];

  return validData.includes(data);
}

/*
  @desc   showing result
  @route  POST /api/v1/players/result/:roomId
  @access private
*/
async function result(req, res) {
  const { roomId } = req.params;

  if (isNaN(parseInt(roomId))) {
    return response.error(res, "Invalid roomId");
  }

  try {
    const room = await models.Room.findByPk(roomId, {
      include: {
        model: models.Game,
        as: "game",
      },
    });

    if (!room) {
      throw new ValidationError("Room not found");
    }

    const max_player = await models.Player.findAll({
      where: {
        room_id: room.id,
      },
    });

    if (max_player.length !== room.game.max_player) {
      throw new ValidationError("room still waiting for players");
    }

    const players = await models.Player.findAll({
      where: {
        room_id: roomId,
      },
      limit: room.game.max_player,
    });

    const winner = determineResult(room, players);

    // updating room status when room reach maximum players
    if (room.isActive) {
      await sequelize.transaction(async (transaction) => {
        await room.update({
          isActive: false,
        }, {
          transaction
        });

        if(winner){
          await models.UserGameHistory.create({
            user_id: winner.id,
            game_id: room.game_id,
            score: 10,
            played_at: Date.now(),
          }, {
            transaction
          })
        }
      })
    }

    return response.success(res, "result fetched successfully", {
      winner: winner !== null ? winner.player_id : null,
      game: room.game.name
    });
  } catch (error) {
    return response.error(res, error);
  }
}

function determineResult(room, players) {
  if (room.game.id === 1) {
    const [player1, player2] = players;

    if (player1.data.toLowerCase() == "r") {
      if (player2.data.toLowerCase() == "p") {
        return player2;
      } else if (player2.data.toLowerCase() == "s") {
        return player1;
      } else {
        return null;
      }
    } else if (player1.data.toLowerCase() == "p") {
      if (player2.data.toLowerCase() == "s") {
        return player2;
      } else if (player2.data.toLowerCase() == "r") {
        return player1;
      } else {
        return null;
      }
    } else if (player1.data.toLowerCase() == "s") {
      if (player2.data.toLowerCase() == "r") {
        return player2;
      } else if (player2.data.toLowerCase() == "p") {
        return player1;
      } else {
        return null;
      }
    }
  }
}

module.exports = {
  fighting,
  result,
};
