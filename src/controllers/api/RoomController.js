const { models, sequelize } = require("../../../models/index");
const response = require("../../utils/response");

/*
  @desc   create room
  @route  POST /api/v1/rooms
  @access public
*/
async function create(req, res) {
  const { game_id } = req.body;

  if (!game_id) {
    return response.error(res, "Missing credentials");
  }

  try {
    const room = await models.Room.create({
      game_id,
      created_by: req.user.id,
    }, {
      returning: true,
    });

    return response.success(res, "room created successfully", room);
  } catch (error) {
    return response.error(res, error);
  }
}

module.exports = {
  // index,
  // show,
  create,
  // update,
  // destroy,
};
