const { models } = require("../../../models/index");
const bcrypt = require("bcryptjs");

/*
  @desc   login page
  @route  GET /login
  @access public
*/
function login(req, res) {
  res.render("home/login", {
    title: "Login",
  });
}

/*
  @desc   attempt to login
  @route  POST /login
  @access public
*/
async function attemptLogin(req, res) {
  const { username, password } = req.body;
  if (!username || !password) {
    res.render("home/login", {
      title: "Login",
      error: {
        type: "warning",
        message: "Missing credentials",
      },
      data: {
        username,
      },
    });
  }

  try {
    const user = await models.User.findOne({
      where: {
        username,
        role: "admin",
      },
    });

    if (!user || !(await bcrypt.compare(password, user.password))) {
      throw new Error("Invalid credentials");
    }

    // save the user to session
    req.session.regenerate((err) => {
      if (err) throw new Error(err.message);

      req.session.user = user;

      req.session.save((err) => {
        if (err) throw new Error(err.message);

        return res.redirect("/");
      });
    });
  } catch (error) {
    return res.render("home/login", {
      title: "Login",
      error: {
        type: "danger",
        message: error.message,
      },
      data: {
        username,
      },
    });
  }
}

/*
  @desc   index page
  @route  GET /
  @access public
*/
async function index(req, res) {
  try {
    const users = await models.User.findAll({
      include: {
        model: models.UserBio,
        as: "bio",
      },
      order: [["id", "asc"]],
    });

    const games = await models.Game.findAll({
      order: [["id", "asc"]],
    });

    // checking message from session
    if (!req.session.message) {
      return res.render("home/index", {
        title: "Homepage",
        user: req.session.user,
        users,
        games,
      });
    }

    const error = req.session.message;
    delete req.session.message;
    req.session.save((err) => {
      if (err) throw new Error(err);
      return res.render("home/index", {
        title: "Homepage",
        user: req.session.user,
        users,
        games,
        error,
      });
    });
  } catch (error) {
    return res.render("home/index", {
      title: "Homepage",
      user: req.session.user,
      users: [],
      games: [],
      error: {
        type: "danger",
        message: error.message,
      },
    });
  }
}

/*
  @desc   logout user
  @route  POST /logout
  @access private
*/
function logout(req, res) {
  // destory session
  req.session.user = null;
  req.session.save((err) => {
    if (err) return res.redirect("/");

    req.session.regenerate((err) => {
      if (err) return res.redirect("/");
    });

    return res.redirect("/login");
  });
}

module.exports = {
  login,
  attemptLogin,
  index,
  logout,
};
