const { models, sequelize } = require("../../../models/index");
const bcrypt = require("bcryptjs");

/*
  @desc   index page
  @route  GET /users
  @access private
*/
function index(req, res) {
  res.render("user/index", {
    title: "Create User",
  });
}

/*
  @desc   create user
  @route  POST /users
  @access private
*/
async function create(req, res) {
  const { username, password, first_name, last_name, hobby, address } =
    req.body;

  if (!username || !password || !first_name || !last_name) {
    return res.render("user/index", {
      title: "Create User",
      error: {
        type: "warning",
        message: "Missing credentials",
      },
      data: {
        username,
        first_name,
        last_name,
        hobby,
        address,
      },
    });
  }

  try {
    const user = await models.User.findOne({
      where: {
        username,
      },
    });

    if (user) {
      throw new Error("Username already exists");
    }

    const salt = await bcrypt.genSalt(parseInt(process.env.SALT));
    const hashPassword = await bcrypt.hash(password, salt);

    await sequelize.transaction(async (transaction) => {
      const newUser = await models.User.create(
        {
          username,
          password: hashPassword,
          role: "player",
        },
        {
          transaction,
          returning: true,
        }
      );

      await models.UserBio.create(
        {
          user_id: newUser.id,
          first_name,
          last_name,
          hobby,
          address,
        },
        {
          transaction,
        }
      );
    });

    res.redirect("/");
  } catch (error) {
    return res.render("user/index", {
      title: "Create User",
      error: {
        type: "danger",
        message: error.message,
      },
      data: {
        username,
        first_name,
        last_name,
        hobby,
        address,
      },
    });
  }
}

/*
  @desc   show user
  @route  GET /users/:id
  @access private
*/
async function show(req, res) {
  if (!req.params.id) {
    return res.redirect("/");
  }
  try {
    const user = await models.User.findByPk(req.params.id, {
      include: {
        model: models.UserBio,
        as: "bio",
      },
    });

    if (!user) {
      req.session.message = {
        type: "warning",
        message: "User not found",
      };
      return res.redirect("/");
    }

    return res.render("user/update", {
      title: "Update User",
      data: user,
    });
  } catch (error) {
    res.render("user/update", {
      title: "Update User",
      error: {
        type: "warning",
        message: error.message,
      },
    });
  }
}

/*
  @desc   update user
  @route  PUT /users/:id
  @access private
*/
async function update(req, res) {
  if (!req.params.id) {
    req.session.message = {
      type: "warning",
      message: "User id is required",
    };

    return res.redirect("/");
  }

  const { first_name, last_name, hobby, address, username } = req.body;

  try {
    await sequelize.transaction(async (transaction) => {
      const userBio = await models.UserBio.findOne({
        where: {
          user_id: req.params.id,
        },
        transaction,
        lock: true,
      });

      if (!userBio) {
        throw new Error("UserBio not found");
      }

      await models.UserBio.update(
        {
          first_name,
          last_name,
          hobby,
          address,
        },
        {
          where: {
            id: userBio.id,
          },
          transaction,
        }
      );
    });

    req.session.message = {
      type: "success",
      message: username + " has been updated successfully",
    };

    return res.redirect("/");
  } catch (error) {
    req.session.message = {
      type: "danger",
      message: error.message,
    };

    return res.redirect("/");
  }
}

/*
  @desc   delete user
  @route  DELETE /users/:id
  @access private
*/
async function destory(req, res) {
  if (!req.params.id) {
    req.session.message = {
      type: "warning",
      message: "User id is required",
    };
    return res.redirect("/");
  }

  try {
    await sequelize.transaction(async (transaction) => {
      const userBio = await models.UserBio.destroy({
        where: {
          user_id: req.params.id,
        },
        transaction,
      });

      if (userBio === 0) {
        throw new Error("User not found");
      }

      await models.UserGameHistory.destroy({
        where: {
          user_id: req.params.id,
        },
        transaction
      })

      await models.User.destroy({
        where: {
          id: req.params.id,
        },
        transaction,
      });
    });

    req.session.message = {
      type: "success",
      message: req.body.username + " has been deleted successfully",
    };

    return res.redirect("/");
  } catch (error) {
    req.session.message = {
      type: "danger",
      message: error.message,
    };
    return res.redirect("/");
  }
}

/*
  @desc   create user game history
  @route  DELETE /users/history
  @access private
*/
async function userHistory(req, res) {
  const { user_id, game_id, score } = req.body;

  if (!user_id || !game_id || !score) {
    req.session.message = {
      type: "warning",
      message: "Missing credentials",
    };

    return res.redirect("/");
  }

  try {
    const user = await models.User.findOne({
      where: {
        id: user_id,
      },
    });

    if (!user) {
      throw new Error("User not found");
    }

    const game = await models.Game.findOne({
      where: {
        id: game_id,
      },
    });

    if (!game) {
      throw new Error("Game not found");
    }

    await models.UserGameHistory.create(
      {
        user_id,
        game_id,
        score,
        played_at: new Date(),
      },
      {
        returning: true,
      }
    );

    req.session.message = {
      type: "success",
      message: user.username + " has played " + game.name,
    };

    return res.redirect("/");
  } catch (error) {
    req.session.message = {
      type: "error",
      message: error.message,
    };

    return res.redirect("/");
  }
}

/*
  @desc   showing user game history
  @route  GET /users/:id/history
  @access private
*/
async function showHistory(req, res) {
  if (!req.params.id) {
    req.session.message = {
      type: "warning",
      message: "User id is required",
    };

    return res.redirect("/");
  }

  try {
    const user = await models.User.findByPk(req.params.id, {
      include: [
        {
          model: models.UserGameHistory,
          as: "game_histories",
          attributes: {
            exclude: ["id", "game_id", "user_id"],
          },
          include: [
            {
              model: models.Game,
              as: "game",
            },
          ],
        },
      ],
      attributes: {
        exclude: ["password"],
      },
      order: [
        [{ model: models.UserGameHistory, as: "game_histories" }, "played_at", "DESC"],
      ],
    });

    if (!user) {
      req.session.message = {
        type: "warning",
        message: "User not found",
      };

      return res.redirect("/");
    }

    res.render("user/histories", {
      title: "User History",
      user,
    });
  } catch (error) {
    req.session.message = {
      type: "danger",
      message: error.message,
    };
  }
}

module.exports = {
  index,
  create,
  show,
  update,
  destory,
  userHistory,
  showHistory,
};
