const router = require("express").Router();
const PlayerController = require("../../controllers/api/PlayerController");
const authenticateAPI = require("../../middlewares/authenticateAPI");

router.post("/fight/:roomId", authenticateAPI, PlayerController.fighting);
router.get("/result/:roomId", authenticateAPI, PlayerController.result);

module.exports = router;
