const router = require("express").Router();
const RoomController = require("../../controllers/api/RoomController");
const authenticateAPI = require("../../middlewares/authenticateAPI");

router.post("/", authenticateAPI, RoomController.create);

module.exports = router;
