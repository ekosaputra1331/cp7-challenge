const router = require("express").Router();
const UserController = require("../../controllers/api/UserController");

router.get("/", UserController.index);
router.get("/:id", UserController.show);
router.post("/", UserController.create);
router.post("/login", UserController.login);
router.put("/:id", UserController.update);
router.delete("/:id", UserController.destroy);

module.exports = router;
