const router = require("express").Router();
const HomeController = require("../../controllers/web/HomeController");
const { isAuthenticated, isLoggedIn } = require("../../middlewares/authenticate");

router.get("/", isAuthenticated, HomeController.index);
router.get("/login", isLoggedIn, HomeController.login);
router.post("/login", isLoggedIn, HomeController.attemptLogin);
router.post("/logout", isAuthenticated, HomeController.logout);

module.exports = router;
