const util = require("util");
const jwt = require("jsonwebtoken");

const jwtVerifyAsync = util.promisify(jwt.verify);
const ValidationError = require("../utils/ValidationError");
const { models } = require("../../models/index");

async function authenticateAPI(req, res, next) {
    // getting token
    const header = req.headers.authorization;
    const token = header && header.split(" ")[1];
    
    try {
        if (!token) {
            throw new ValidationError("No Token");
        }
        // verifying token
        const { sub } = await jwtVerifyAsync(token, process.env.JWT_SECRET);

        // getting user by id
        const user = await models.User.findByPk(sub);

        if(!user){
          throw new ValidationError("Invalid credentials");
        }

        req.user = user;
        next();
    } catch (error) {
        next(error);
    }
}

module.exports = authenticateAPI;
