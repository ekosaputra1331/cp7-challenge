class ValidateError extends Error {
    constructor(message, code = 400){
      super(message);
      this.code = code;
      this.name = 'ValidateError';
    }
  }
  
  module.exports = ValidateError;
  