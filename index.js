require("colors");
require("dotenv").config();
const express = require("express");
const morgan = require("morgan");
const path = require("path");
const layout = require("express-ejs-layouts");
const session = require("express-session");
const methodOverride = require("method-override");


// initialize
const app = express();
const PORT = process.env.PORT || 3000;

// middlewares
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use("/", express.static(path.join(__dirname, "public")));
app.use(morgan("dev"));
app.use(session({
  secret: process.env.SECRET_SESSION,
  resave: false,
  saveUninitialized: false
}))
app.use(methodOverride('_method'));


// set view engine
app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "views"));
app.use(layout);

// routes web
app.use("/", require("./src/routes/web/homeRoute"));
app.use("/users", require("./src/routes/web/userRoute"));

// routes api
app.use("/api/v1/users", require("./src/routes/api/userRoute"))
app.use("/api/v1/games", require("./src/routes/api/gameRoute"))
app.use("/api/v1/rooms", require("./src/routes/api/roomRoute"))
app.use("/api/v1/players", require("./src/routes/api/playerRoute"))

// 404
app.use(require('./src/utils/response').notFoundHandler);

// error
app.use(require('./src/utils/response').errorHandler);

app.listen(PORT, () => {
  console.log(`Server listening on http://${process.env.HOST}:${PORT}`.cyan);
});
