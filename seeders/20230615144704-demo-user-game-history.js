'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    return await queryInterface.bulkInsert("user_game_histories", [
      {
        user_id: 2,
        game_id: 1,
        score: 70,
        played_at: new Date(),
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        user_id: 2,
        game_id: 2,
        score: 80,
        played_at: new Date(),
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        user_id: 3,
        game_id: 3,
        score: 75,
        played_at: new Date(),
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
