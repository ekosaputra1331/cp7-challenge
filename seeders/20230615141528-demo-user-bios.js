"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */

    return await queryInterface.bulkInsert("user_bios", [
      {
        user_id: 1,
        first_name: 'super',
        last_name: 'admin',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        user_id: 2,
        first_name: 'eko',
        last_name: '-',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        user_id: 3,
        first_name: 'saputra',
        last_name: '-',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    return await queryInterface.bulkDelete('user_bios', null, {});
  },
};
