"use strict";

require('dotenv').config();
const bcrypt = require("bcryptjs");

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    return await queryInterface.bulkInsert("users", [
      {
        username: "admin",
        password: await bcrypt.hash('admin123', parseInt(process.env.SALT)),
        role: "admin",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        username: "eko",
        password: await bcrypt.hash('eko123', parseInt(process.env.SALT)),
        role: "player",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        username: "saputra",
        password: await bcrypt.hash('eko123', parseInt(process.env.SALT)),
        role: "player",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    return await queryInterface.bulkDelete('users', null, {});
  },
};
