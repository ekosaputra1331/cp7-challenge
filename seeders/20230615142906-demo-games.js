'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    return await queryInterface.bulkInsert("games", [
      {
        name: "Rock-Paper-Scissor",
        description: 'this is the traditional game',
        max_player: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Gasing",
        description: 'funny game to play with friends',
        max_player: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Layangan",
        description: 'challeging game to play with friends',
        max_player: 4,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    return await queryInterface.bulkDelete('games', null, {});
  }
};
