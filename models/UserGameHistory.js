'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserGameHistory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      UserGameHistory.belongsTo(models.Game, {
        foreignKey: "game_id",
        as: "game",
      });
      
    }
  }
  UserGameHistory.init({
    user_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'User'
      },
      key: 'id'
    },
    game_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'Game',
      },
      key: 'id'
    },
    score: DataTypes.INTEGER,
    played_at: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'UserGameHistory',
    tableName: 'user_game_histories'
  });
  return UserGameHistory;
};