'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserBio extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  UserBio.init({
    user_id: {
      type: DataTypes.NUMBER,
      references: {
        model: 'User',
        key: 'id'
      }
    },
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    hobby: DataTypes.STRING,
    address: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'UserBio',
    tableName: 'user_bios'
  });
  return UserBio;
};