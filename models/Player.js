'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Player extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Player.init({
    room_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'Room',
        key: 'id'
      }
    },
    player_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'Player',
        key: 'id'
      }
    },
    data: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Player',
    tableName: 'players'
  });
  return Player;
};