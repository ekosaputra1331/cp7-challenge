'use strict';

/**
 * @typedef {import('sequelize').Model} SequelizeModel
 * @typedef {import('sequelize').DataTypes} DataTypes
 */

/**
 * @param {SequelizeModel} sequelize
 * @param {DataTypes} DataTypes
 * @returns {SequelizeModel}
 */

const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User.hasOne(models.UserBio, {
        foreignKey: 'user_id',
        as: 'bio'
      });

      User.hasMany(models.UserGameHistory, {
        foreignKey: "user_id",
        as: "game_histories",
      });      
    }
  }
  User.init({
    username: {
      type: DataTypes.STRING,
      unique: true,
    },
    password: DataTypes.TEXT,
    role: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User',
    tableName: 'users'
  });
  return User;
};